package be.antwerpmedia.radioparkfm;

public class Config {

    //if admin panel disabled, put your radio stream url here
    public static final String RADIO_STREAM_URL = "http://shoutcast.movemedia.eu/parkfm";

    //set true to enable admin panel or set false to disable
    public static final boolean ENABLE_ADMIN_PANEL = false;
    //if admin panel enabled, put your admin panel url here
    public static final String ADMIN_PANEL_URL = "http://10.0.2.2/your_single_radio";

    //ads configuration
    public static final boolean ENABLE_ADMOB_BANNER = false;
    public static final boolean ENABLE_ADMOB_INTERSTITIAL_ON_LOAD = false;
    public static final boolean ENABLE_ADMOB_INTERSTITIAL_ON_PLAY = false;
    public static final boolean ENABLE_ADMOB_INTERSTITIAL_ON_DRAWER_SELECTION = false;
    public static final int ADMOB_INTERSTITIAL_ON_PLAY_INTERVAL = 3;

    //auto play function
    public static final boolean ENABLE_AUTO_PLAY = true;

    //layout customization
    public static final boolean ENABLE_ALBUM_ART = true;
    public static final boolean ENABLE_SOCIAL_MENU = false;
    public static final boolean ENABLE_VISUALIZER = false;
    /* note : if visualizer is enabled, radio image background will be replaced with visualizer */

    //set default volume to listening radio streaming, volume range 0 - 15
    public static final boolean ENABLE_VOLUME_BAR = false;
    public static final int DEFAULT_VOLUME = 11;

    //splash screen duration in millisecond
    public static final int SPLASH_SCREEN_DURATION = 3000;

}