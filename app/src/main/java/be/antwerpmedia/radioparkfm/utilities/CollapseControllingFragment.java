package be.antwerpmedia.radioparkfm.utilities;

public interface CollapseControllingFragment {
    boolean supportsCollapse();
}
