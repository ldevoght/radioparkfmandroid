package be.antwerpmedia.radioparkfm.utilities;

import be.antwerpmedia.radioparkfm.models.ItemPrivacy;
import be.antwerpmedia.radioparkfm.models.ItemRadio;

public class Constant {
    public static ItemRadio itemRadio;
    public static ItemPrivacy itemPrivacy;
}
