package be.antwerpmedia.radioparkfm.utilities;

public interface PermissionsFragment {
    String[] requiredPermissions();
}
