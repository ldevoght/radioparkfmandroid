package be.antwerpmedia.radioparkfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import be.antwerpmedia.radioparkfm.Config;
import be.antwerpmedia.radioparkfm.R;

public class ActivitySplash extends AppCompatActivity {

    private InterstitialAd interstitialAd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        if (Config.ENABLE_ADMOB_INTERSTITIAL_ON_LOAD) {
            loadInterstitialAd();
        } else {
            Log.d("AdMob", "Interstitial on load is disabled");
        }

        new CountDownTimer(Config.SPLASH_SCREEN_DURATION, 1000) {
            @Override
            public void onFinish() {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                finish();
                if (Config.ENABLE_ADMOB_INTERSTITIAL_ON_LOAD) {
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                    }
                } else {
                    Log.d("AdMob", "Interstitial on load is disabled");
                }
            }

            @Override
            public void onTick(long millisUntilFinished) {

            }
        }.start();

    }

    private void loadInterstitialAd() {
        Log.d("TAG", "showAd");
        interstitialAd = new InterstitialAd(getApplicationContext());
        interstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial_unit_id));
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {

            }
        });
    }

}