package be.antwerpmedia.radioparkfm.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import be.antwerpmedia.radioparkfm.R;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class ActivityRadioParkInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radioparkinfo);

        if (Build.VERSION.SDK_INT >= 26) {
            TextView txtView = (TextView)findViewById(R.id.txtRadioParkInfo);
            txtView.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }

        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    public void setupToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle(R.string.drawer_more);
        }
    }



}
